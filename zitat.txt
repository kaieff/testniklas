Fühl dich nicht verantwortlich für Menschen, die sich selbst verantworten können. Schenke dir Liebe, so dass du Liebe weiterverschenken kannst. Das, was du musst, ist das, was du darfst. Das, was du darfst, ist das, was du wirkich bist.

Liebe geht raus!!!